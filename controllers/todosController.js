var mongoose = require('mongoose')
var todo = require('../models/todo');
var user = require('../models/user')
var { successResponse, errorResponse } = require('../helpers/response')


exports.getAllToDo = (req, res) => {

  todo.find().exec(function (err, todo) {
    if (err) throw err;
    res.status(200).send(todo);
    console.log(todo)
  })
}

exports.getAllToDoawait = async (req, res) => { //await

  try{
    let hasil = await todo.find()
    console.log(hasil);
  } catch {
    res.status(500).send(err)
  }
}

exports.getAllToDopromise = (req, res) => { //promise
  todo.find().exec()
  .then((todo) => {
    console.log(todo)
    res.json({
      result: todo
    })
  })
  .catch((err) => {
    res.status(422).json({ success: false, err: err })
  })
}

exports.getAllToDoanduser = (req, res) => { //promise

  todo.findOne({ title: req.params.title }).populate({ path: 'user', select: ['username', 'name'] })
    .exec()
    .then((todo) => {
      res.json({ results: todo })
    })
    .catch((err) => {
      res.status(422).json({ success: false, err: err })
    })
}

exports.getByTitle = (req, res) => {

  var title = req.params.title;

  todo.find({
    title: title
  }).exec(function (err, todo) {
    if (err) throw err;
    res.status(200).send(todo);
    console.log(todo);
  })
}

exports.postNewTodo = (req, res) => { //promise
  var title = req.body.title;
  var ket = req.body.ket;
  var dueDate = req.body.dueDate;
  var priority = req.body.priority;
  var status = req.body.status;

  let newtodo = new todo({
    title: title,
    ket: ket,
    dueDate: dueDate,
    priority: priority,
    status: status
  })
  newtodo.save().then((newtodo) => {
    res.json(
      successResponse('New ToDo has been add to your todo!', newtodo)
    )
  })
    .catch((err) => {
      res.json(
        errorResponse('Sorry, failed', err, 500)
      )
    })
}

exports.postNewTodoasync = async (req, res) => { //using  await
  console.log(req.body)
  var _id = new mongoose.Types.ObjectId()
  var title = req.body.title;
  var ket = req.body.ket;
  var dueDate = req.body.dueDate;
  var priority = req.body.priority;
  var status = req.body.status;

  let newtodo = new todo({
    _id: _id,
    title: title,
    ket: ket,
    dueDate: dueDate,
    priority: priority,
    status: status
  });

  try {

    let todonew = newtodo.save();
    console.log(todonew);


  } catch(err){
    res.status(500).send(err);
  }

}

exports.postNewTodoanduser = (req, res) => {

  user.findOne({ username: req.params.username })
    .exec((err, users) => {
      if (err) {
        res.status(422).json({ success: false, err: err })
      }
      let newtodo = new todo({
        _id: new mongoose.Types.ObjectId(),
        title: req.body.title,
        ket: req.body.ket,
        dueDate: req.body.dueDate,
        priority: req.body.priority,
        status: req.body.status,
        user: users._id
      })
      newtodo.save((err) => {

        if (err) {
          res.status(422).json({ success: false, err: err })
        }
        user.updateOne(
          { username: req.params.username },
          { $push: { todo: newtodo._id } }
        ).exec()
          .then((users) => {
            console.log('Success')
          })
        res.status(200).json({ result: newtodo })
      })
    })
}

exports.deleteByTitle = (req, res) => {

  var title = req.params.title;

  todo.deleteOne({
    title: title
  }).exec(function (err, todo) {
    if (err) throw err;
    console.log(todo)
  })
}

exports.deleteByTitlepromise = (req, res) => { //promise

  todo.deleteOne({title: req.params.title}).exec()
  .then((todo) => {
    res.json({
      result: todo
    })
  })
  .catch((err) => {
    res.status(500).send(err)
  })
}

exports.deleteByTitleawait = async (req, res) => { //await
  try{
    let hapus = await todo.deleteOne({title: req.params.title})
    res.json('list successfully deleted!')
  }
  catch{
    res.status(500).send(err)
  }
}

exports.updatebyTitle = (req, res) => {

  var title = req.params.title;

  todo.findOneAndUpdate({
    title: title
  }, {
      $set: req.body
    }, {new: true}, function (err, todo) {
      if (err) throw err;
      res.status(200).send(todo);
      console.log(todo)
    })
};

exports.updatebyTitlepromise = (req, res) => { //promise
  todo.findOneAndUpdate({
    title: req.params.title
  }, {
    $set: req.body
  }).exec()
  .then((todo) => {
    console.log(todo)
  })
  .catch((err) => {
    res.status(500).send(err)
  })
}

exports.updatebyTitleawait = async (req, res) => {
  try {
    let ubah = await todo.findOneAndUpdate(
      {title: req.params.title}, 
      {$set: req.body})
    console.log(`list succesfully updated! ${todo}`)
  }
  catch{
    res.status(500).send(err)
  }
}


