var mongoose = require('mongoose')
var user = require('../models/user')
var bcrypt = require('bcrypt')
var jwt = require('jsonwebtoken')
var saltRounds = 10;
var { successResponse, errorResponse } = require('../helpers/response')

exports.auth = (req, res, next) => { //login
    user.findOne({
        username: req.body.username
    }).exec(function (err, user) {
        if (err) throw err

        bcrypt.compare(req.body.password, user.password).then(function (result) {

            if(result) {
                var token = jwt.sign(user.toJSON(), process.env.SECRET_KEY, {
                    algorithm: 'HS256',
                    expiresIn: "4h"
                })

                res.json({
                    message: 'Login Success',
                    token: token,
                    success: true
                })
            } else {
                res.status(401)
                res.json({
                    message: 'Password Incorrect',
                    success: false
                })
            }

        }) .catch((err) => { return next(err) })
    })
}

exports.addUser = (req, res) => {
    console.log(req.body)
    var _id = new mongoose.Types.ObjectId()
    var username = req.body.username
    var firstName = req.body.firstName
    var lastName = req.body.lastName
    var gender = req.body.gender
    var email = req.body.email
    var password = req.body.password
    var todo = []

    var newUser = new user({
        _id: _id,
        username: username,
        name: {
            firstName: firstName,
            lastName: lastName
        },
        gender: gender,
        email: email,
        password: password,
        todo: todo
    });
    newUser.save(function (err) {
        if (err) throw err

        return res.status(201).json(
            successResponse('User successfully saved', newUser)
        )
    });
}

exports.getUser = (req, res) => {

    user.find().exec()
        .then((users) => {
            res.json(
                successResponse('User found', users)
            )
        })
        .catch((err) => {
            res.json(
                errorResponse('User not found', err, users)
            )
        })
}

exports.getAlluserandtodo = (req, res) => {

    user.findOne({username: req.params.username}).populate('ToDo')
    .exec()
    .then((users) => {
      res.json({results: users})
    })
    .catch((err) => {
      res.status(422).json({success: false, err: err})
    })
  }

exports.getUserByUname = (req, res) => {
    var uname = req.params.uname

    user.find({
        username: uname
    }).exec(function (err, users) {
        if (err) throw err

        res.json(
            successResponse('User found', users)
        )
        console.log(users)
    })
}

exports.getUserById = (req, res) => {
    var id = req.params.id

    user.findById(id, function (err, users) {
        if (err) throw err

        res.json(
            successResponse('User found', users)
        )
        console.log(users)
    })
}

exports.updateUser = (req, res) => {

    user.findByIdAndUpdate(req.params.id, {
        $set: req.body
    }, function (err, users) {
        if (err) throw err

        res.json(
            successResponse('User updated successfully', users)
        )
        console.log(users)
    })
}



exports.deleteUserByUname = (req, res) => {
    var uname = req.params.uname

    user.deleteOne({
        username: uname
    }).exec(function (err, users) {
        if (err) throw err

        res.json(
            successResponse('User deleted successfully', users)
        )
        console.log("User deleted successfully")
    })
}




exports.deleteUserById = (req, res) => {

    user.findByIdAndDelete(req.params.id, req.body, function (err, users) {
        if (err) throw err

        res.json(
            successResponse('User deleted successfully', users)
        )
        console.log("User deleted successfully")
    })
}