var jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const saltRounds = 10;

exports.isAuthenticated = function (req, res, next) {
    var token = req.body.token || req.query.token || req.headers.auth //mengambil token diantara request
    if (token) { //jika ada token
        jwt.verify(token, process.env.SECRET_KEY, function (err, decoded) {
            if (err) {
                res.json({ message: 'Failed to authenticate token'})
            }
            else {
                req.decoded = decoded; //menyimpan decoded ke req.decoded
                next(); //melanjutkan proses
            }
        });
    }
    else {
        return req.status(403).send({message: 'No token provided'});
    }
}