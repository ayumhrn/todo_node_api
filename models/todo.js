var mongoose = require('mongoose');
var Schema = mongoose.Schema

var todoSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    title: {
        type: String,
        required: [true, 'it must be have title!']
    },
    ket: String,
    dueDate: {
        type: Date,
        required: [true, 'Your todo must be have due date!']
    },
    priority: {
        type: String,
        enum: ['high', 'low']
    },
    status: {
        type: String,
        enum: ['complete', 'uncomplete']
    },
    user: [{ type: mongoose.Schema.Types.ObjectId, ref: 'user' }]
    },
    {
        collections: 'todo'
    })

var ToDo = mongoose.model('todo', todoSchema);

module.exports = ToDo;  