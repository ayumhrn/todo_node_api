var mongoose = require('mongoose');
var bcrypt = require('bcrypt')
var saltRounds = 10;
var uniqueValidator = require('mongoose-unique-validator')
var schema = mongoose.Schema;


var userSchema = new schema(
  {
    _id: mongoose.Schema.Types.ObjectId,
    username: {
      type: String,
      unique: true,
      required: [true, "It's required"]
    },
    name: {
      firstName: {
        type: String,
        required: [true, "Why you don't have a name?"]
      },
      lastName: String
    },
    gender: {
      type: String,
      enum: ['Male', 'Female']
    },
    email: {
      type: String,
      lowercase: true,
      required: true,
      validate: function(email) {
        return /^[a-zA-Z0-9.!#$%&’*+\/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(email)
      }
    },
    password: {
      type: String,
      required: true
    },
    todo: [{ type: mongoose.Schema.Types.ObjectId, ref: 'ToDo' }]
  },
  {
    collection: 'users'
  }
)

userSchema.pre('save', function (next){
  this.password = bcrypt.hashSync(this.password, saltRounds);
  next();
})

userSchema.plugin(uniqueValidator,{message: 'Error, expected {PATH} to be unique.'});
var user = mongoose.model('user', userSchema);

module.exports = user