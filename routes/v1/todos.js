var express = require('express');
var router = express.Router();
var todoController = require('../../controllers/todosController');

//get
router.get('/', todoController.getAllToDo);
router.get('/:title', todoController.getByTitle);
router.get('/todo/:title', todoController.getAllToDoanduser);
router.get('/todo/await/get', todoController.getAllToDoawait);
router.get('/todo/promise/get', todoController.getAllToDopromise);

//post
router.post('/addtodo', todoController.postNewTodo);
router.post('/addtodo/:username', todoController.postNewTodoanduser);
router.post('/await/addtodo', todoController.postNewTodoasync)

//delete
router.delete('/todo/:title', todoController.deleteByTitle);
// router.delete('/todo/:title', todoController.deleteByTitlepromise);
// router.delete('/todo/:title', todoController.deleteByTitleawait);


//update
router.put('/todo/:title', todoController.updatebyTitle);
// router.put('/todo/:title', todoController.updatebyTitlepromise);
// router.put('/todo/:title', todoController.updatebyTitleawait);

module.exports = router;