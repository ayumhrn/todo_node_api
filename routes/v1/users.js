var express = require('express');
var router = express.Router();
var usersCtrl = require('../../controllers/usersController')
var auth = require('../../middleware/auth');

//Create
router.post('/adduser', usersCtrl.addUser)
router.post('/user/auth/login', usersCtrl.auth)

//Read
router.get('/', usersCtrl.getUser);
router.get('/uname/:uname', auth.isAuthenticated, usersCtrl.getUserByUname)
router.get('/id/:id', usersCtrl.getUserById)
router.get('/:username', usersCtrl.getAlluserandtodo)

//Update
router.put('/:id', auth.isAuthenticated, usersCtrl.updateUser)

//Delete
router.delete('/uname/:uname', usersCtrl.deleteUserByUname)
router.delete('/:id', usersCtrl.deleteUserById)

module.exports = router