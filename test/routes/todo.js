var chai = require('chai');
var chaiHttp = require('chai-http');
var mongoose = require('mongoose')
var expect = chai.expect;

chai.use(chaiHttp);

describe('ToDo function', function(){
    var url = 'http://localhost:3000'

    after(function () {
        mongoose.connection.close();
    })
    it("show all list ToDo should show OK", function (done) {
        chai.request(url)
            .get('/todo')
            .end(function (err, res) {
                expect(res).to.have.status(200)
                expect(res).to.be.a('object')
                done()
            })
    })
    it("show list ToDo by title should show OK", function (done) {
        chai.request(url)
            .get('/todo/${title}')
            .end(function (err, res) {
                expect(res).to.have.status(200)
                expect(res).to.be.a('object')
                done()
            })
    });
    it("add list should show OK", function (done) {
        chai.request(url)
            .post('/todo/addtodo')
            .send({
                title: 'foo',
                priority: 'foo',
                status: 'foo'
            })
            .end(function (err, res) {
                expect(res).to.have.status(200)
                done()
            })
    });
    it("update list should show OK", function (done) {
        chai.request(url)
            .put('/todo/todo/swimming')
            .send({
                title: 'foo',
                priority: 'foo',
                status: 'foo'
            })
            .end(function (err, res) {
                expect(res).to.have.status(200)
                done()
            })
    });
    it("delete list by title should show OK", function () {
        chai.request(url)
            .delete('/todo/todo/${title}')
            .end(function (err, res) {
                expect(res).to.have.status(200)
            })
    });
});