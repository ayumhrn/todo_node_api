var chai = require('chai')
var mongoose = require('mongoose')
var chaiHttp = require('chai-http')
var expect = chai.expect

chai.use(chaiHttp)

describe('Users Function', function () {
    var url = 'http://localhost:3000'

    before(function () {
        mongoose.set('useNewUrlParser', true)
        mongoose.connect('mongodb://localhost/ToDo', function (err) {
            if (err) throw (err)
            console.log('Database connection successful')
        })
        userCtrl = require('./../../controllers/usersController');
    })

    after(function () {
        mongoose.connection.close();
    })

    it("add users should show OK", function (done) {
        chai.request(url)
            .post('/users/adduser')
            .send({
                username: 'foo',
                firstName: 'foo',
                email: 'foo@mail.com'
            })
            .end(function (err, res) {
                expect(res).to.have.status(201)
                done()
            })
    })

    it("show all users should show OK", function (done) {
        chai.request(url)
            .get('/users')
            .end(function (err, res) {
                expect(res).to.have.status(200)
                expect(res).to.be.a('object')
                done()
            })
    })

    it("show user by username should show OK", function (done) {
        chai.request(url)
            .get('/users/uname/${uname}')
            .end(function (err, res) {
                expect(res).to.have.status(200)
                expect(res).to.be.a('object')
                done()
            })
    })

    it("show user by id should show OK", function (done) {
        chai.request(url)
            .get('/users/uname/5d50ef48a6ae9826b537c296')
            .end(function (err, res) {
                expect(res).to.have.status(200)
                expect(res).to.be.a('object')
                done()
            })
    })

    it("update user should show OK", function (done) {
        chai.request(url)
            .put('/users/5d50ef48a6ae9826b537c296')
            .send({
                username: 'foo',
                firstName: 'foo',
                email: 'foo@mail.com'
            })
            .end(function (err, res) {
                expect(res).to.have.status(200)
                done()
            })
    })


    it("delete user by username should show OK", function (done) {
        chai.request(url)
            .delete('/users/uname/${uname}')
            .end(function (err, res) {
                expect(res).to.have.status(200)
                done()
            })
    })

    it("delete user by id should show OK", function (done) {
        chai.request(url)
            .delete('/users/5d50ef48a6ae9826b537c296')
            .end(function (err, res) {
                expect(res).to.have.status(200)
                done()
            })
    })
    
})

